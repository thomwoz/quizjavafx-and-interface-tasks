package Interfejsy1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Cat cat1=new Cat();
        Cat cat2=new Cat();
        Dog dog1=new Dog();
        Dog dog2=new Dog();

        List<Voice> listOfCatsAndDogs=new ArrayList<>();

        listOfCatsAndDogs.add(cat1);
        listOfCatsAndDogs.add(dog1);
        listOfCatsAndDogs.add(cat2);
        listOfCatsAndDogs.add(dog2);

        for(Voice p:listOfCatsAndDogs) {
            p.giveSound();
        }
    }
}
