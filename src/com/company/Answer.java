package com.company;

public enum Answer {

    TAK(true),YES(true),yes(true), tak(true), si(true), oui(true),yep(true), da(true), hai(true),
    NIE(),nie(), no(), nein(),NO(),niet(),non(),nope();

    private boolean isTrue=false;

    public boolean isTrue() {
        return isTrue;
    }

    Answer(){}

    Answer(boolean isTrue) {
        this.isTrue=isTrue;
    }

}
