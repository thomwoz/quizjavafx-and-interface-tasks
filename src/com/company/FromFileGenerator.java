package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FromFileGenerator implements QuestionGenerator {
    private String fileName = "questions.txt";
    private static List<Question> questionList=new ArrayList<>();

    public static void createQuestion(String s) {
    //    int z=0;
    //    while(s.charAt(s.length()-2-z)!=' ') z++;
    //    System.out.println(z);

        String question=s.substring(0,s.indexOf('?')+1);
        boolean trueOrFalse;

        String answer = s.substring(s.indexOf('?')+2);

        trueOrFalse=Answer.valueOf(answer).isTrue();
        questionList.add(new Question(question,trueOrFalse));
    }
    @Override
    public List<Question> generateQuestions() {

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(FromFileGenerator::createQuestion);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return questionList;
    }

}

