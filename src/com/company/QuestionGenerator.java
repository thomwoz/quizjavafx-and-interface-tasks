package com.company;

import java.util.List;

public interface QuestionGenerator {

    //public
    List<Question> generateQuestions();

}
