package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class QuizFrame extends JFrame implements ActionListener {

    private QuestionGenerator questionGenerator;
    private List<Question> questionList=new ArrayList<Question>();
    private int currentQuestion;
    private JLabel jLabel;
    private int numberOfPoints=0;



    public void setQuestionGenerator(QuestionGenerator questionGenerator) {
        this.questionGenerator = questionGenerator;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }


    public QuizFrame(){
        setQuestionGenerator(new FromFileGenerator());

        setQuestionList(questionGenerator.generateQuestions());

        setDefaultFrameProperties();

        addLabelAndButtons();
    }

    private void addLabelAndButtons() {
        jLabel=new JLabel(questionList.get(currentQuestion).getText());
        jLabel.setHorizontalAlignment(JLabel.CENTER);
        JButton yesButton=new JButton(Answer.TAK.toString());
        yesButton.addActionListener(this);
        JButton noButton=new JButton(Answer.NIE.toString());
        noButton.addActionListener(this);
        add(jLabel);
        add(yesButton);
        add(noButton);
    }

    private void setDefaultFrameProperties() {
        setSize(500,500);
        setTitle("Quiz");
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(3,1));
    }



    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();
        if(Answer.valueOf(button.getText()).isTrue()){
            if(questionList.get(currentQuestion).isCorrect()) numberOfPoints++;
        }
        if(!Answer.valueOf(button.getText()).isTrue()) {
            if(!questionList.get(currentQuestion).isCorrect()) numberOfPoints++;
        }
        if(currentQuestion<questionList.size()-1) currentQuestion++;
        else {
            JOptionPane.showMessageDialog(null, "Zdobyłeś " + numberOfPoints + " punktów");
            System.exit(0);
        }
        jLabel.setText(questionList.get(currentQuestion).getText());
    }
}
