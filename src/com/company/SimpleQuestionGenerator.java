package com.company;

import java.util.ArrayList;
import java.util.List;

public class SimpleQuestionGenerator implements QuestionGenerator {

    @Override
    public List<Question> generateQuestions() {
        List<Question> questions=new ArrayList<>();
        questions.add(new Question("Odpowiedz tak?", true));
        questions.add(new Question("Czy tak czy nie(nie)?", false));
        questions.add(new Question("Czy tak czy nie(nie)?", false));
        questions.add(new Question("Czy tak czy nie(tak)?", true));
        questions.add(new Question("Czy tak czy nie(nie)?", false));
        questions.add(new Question("Czy tak czy nie(tak)?", true));
        return  questions;
    }

}
