package interfejsy2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Employee emp1=new Employee("Darek", "Michalczewski","Manager");
        Employee emp2=new Employee("Andrzej", "Wózkoski","Manager");
        Employee emp3=new Employee("Marek", "Andrzejewski","Manager");
        Employee emp4=new Employee("Wojciech", "Wózkoski","Manager");

        List<Person> personList=new ArrayList<>();
        personList.add(emp1);
        personList.add(emp4);
        personList.add(emp2);
        personList.add(emp3);

        for(Person p:personList){
            System.out.println(p);
        }
        System.out.println();

        Collections.sort(personList);

        for(Person p:personList){
            System.out.println(p);
        }
    }

}
