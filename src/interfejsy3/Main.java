package interfejsy3;

public class Main {

    public static void main(String[] args) {
        Person person=new Person(new Caching());
        person.save();
        person.setInfSaver(new SavingToDatabase());
        person.save();
    }
}
