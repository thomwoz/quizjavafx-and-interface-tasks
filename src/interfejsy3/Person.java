package interfejsy3;

public class Person{

    InformationSaver infSaver;

    public void setInfSaver(InformationSaver infSaver) {
        this.infSaver = infSaver;
    }

    public Person(InformationSaver infSaver) {
        this.infSaver = infSaver;
    }

    public void save() {
//        setInfSaver(new Caching());
        infSaver.save();
    }

}
