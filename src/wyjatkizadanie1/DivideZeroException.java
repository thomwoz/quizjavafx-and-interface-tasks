package wyjatkizadanie1;

public class DivideZeroException extends Exception {
    @Override
    public String getMessage() {
        return "Nie można dzielić przez zero";
    }
}
