package wyjatkizadanie1;

public class Main {

    public static void main(String[] args) {

        try {
            System.out.println(Number.divide(13,0));
        } catch (DivideZeroException e) {
            System.out.println(e.getMessage());
        }

        try {
            System.out.println(Number.divide(13,2.5));
        } catch (DivideZeroException e) {
            System.out.println(e.getMessage());
        }
    }
}
