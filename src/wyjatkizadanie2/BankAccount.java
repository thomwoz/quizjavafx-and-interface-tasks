package wyjatkizadanie2;

public class BankAccount {
    private double funds;

    public BankAccount(double money) {
        this.funds = money;
    }

    public void withdraw(double money) throws InsufficentFundsException {
        if(money>funds) throw new InsufficentFundsException();
        else funds-=money;
    }

    public void deposit(double money) {
        funds+=money;
    }
}
