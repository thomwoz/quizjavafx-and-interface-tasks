package wyjatkizadanie2;

public class InsufficentFundsException extends Exception{

    @Override
    public String getMessage() {
        return "Niewystarczające środki na koncie";
    }
}
