package wyjatkizadanie2;

public class Main {
    public static void main(String[] args) {
        BankAccount bankAccount=new BankAccount(7234.57);
        bankAccount.deposit(234.22);
        try {
            bankAccount.withdraw(8000);
        } catch(InsufficentFundsException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Podaj poprawną wartość.");
        }

        try {
            bankAccount.withdraw(5000.25);
            System.out.println("Srodki wypłacone");
        } catch (InsufficentFundsException e) {
            System.out.println(e.getMessage());
        }
    }
}
